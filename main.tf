provider "google" {
  credentials = file("juneway-335712-51a94a0932a7.json")
  project     = "juneway-335712"
  region      = "europe-west3"
  zone        = "europe-west3-a"
}

variable "private_key_path" {
  description = "private key"
}

variable "node_count" {
 default = "3"
}

resource "google_compute_instance" "my_servers" {
  count = "${var.node_count}"
  name         = "node${count.index+1}"
  machine_type = "e2-small"
  boot_disk {
    initialize_params {
      size = "20"
      image = "debian-cloud/debian-10"
    }
  }
  metadata = {
    ssh-keys = "devopssam21:${file("~/.ssh/id_rsa.pub")}"
  }
  connection {
    type     = "ssh"
    user     = "devopssam21"
    private_key = "${file(var.private_key_path)}"
    host     = "${self.network_interface.0.access_config.0.nat_ip}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt install nginx -y",
      "sudo rm -rf /var/www/html/*",
      "sudo chmod 777 /var/www/html",
      "sudo echo '<b> Juneway ${self.network_interface.0.network_ip} ${self.name}</b>' > /var/www/html/index.html"
    ]
  }
  provisioner "local-exec" {
   command = "echo ${self.name} ${self.network_interface.0.access_config.0.nat_ip} >> host.list"
  }
  

  network_interface {
    network = "default"
    access_config {}
  }
}
